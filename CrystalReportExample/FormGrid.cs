﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CrystalReportExample
{
    public partial class FormGrid : Form
    {
        DataBaseDataContext _db = new DataBaseDataContext();
        PrintHelper P = new PrintHelper();
        DataTable dt = new DataTable();
        
        public FormGrid()
        {
            InitializeComponent();
        }

        private void FormGrid_Load(object sender, EventArgs e)
        {
            Listele();
            textBoxSearch.Enabled = false;
        }

        private void Listele()
        {
            var Lst = (from s in _db.Customers select s).ToList();
            
            dt = P.ConvertTo(Lst);
            gridGroupingControl1.DataSource = dt;
        }
        
    }
}
