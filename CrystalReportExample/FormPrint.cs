﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrystalReportExample.Reports;

namespace CrystalReportExample
{
    public partial class FormPrint : Form
    {
        readonly DataBaseDataContext _db = new DataBaseDataContext();
        readonly PrintHelper P = new PrintHelper();
        public string HangiListe;

        public FormPrint()
        {
            InitializeComponent();
        }

        private void FormPrint_Load(object sender, EventArgs e)
        {
            switch (HangiListe)
            {
                case "Customers":
                    CustomersPrint();
                    break;
                case "CustomersFiltered":
                    CustomersFilteredPrint();
                    break;
                default:
                    MessageBox.Show("İstenen koşul sağlanamadı!");
                    break;
            }
        }

        private void CustomersPrint()
        {
            CrystalReport1 crystalReport = new CrystalReport1();
            crystalReportViewer1.ReportSource = crystalReport;
        }

        private void CustomersFilteredPrint()
        {
            CrystalReport1 crystalReport = new CrystalReport1();
            var results = (from s in _db.Customers where s.Country.Contains("Venezuela") select s).ToList();
            if (results != null)
            {
                DataTable table = P.ConvertTo(results);
                crystalReport.SetDataSource(table);
                crystalReportViewer1.ReportSource = crystalReport;
            }
        }

    }
}
