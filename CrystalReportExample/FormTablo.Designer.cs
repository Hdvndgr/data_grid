﻿namespace CrystalReportExample
{
    partial class FormTablo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sfDataGrid1 = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.buttonRapor = new System.Windows.Forms.Button();
            this.buttonCustomers = new System.Windows.Forms.Button();
            this.buttonCustomersFiltered = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.sfDataGrid1)).BeginInit();
            this.SuspendLayout();
            // 
            // sfDataGrid1
            // 
            this.sfDataGrid1.AccessibleName = "Table";
            this.sfDataGrid1.AllowEditing = false;
            this.sfDataGrid1.AllowGrouping = false;
            this.sfDataGrid1.AllowSorting = false;
            this.sfDataGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sfDataGrid1.Location = new System.Drawing.Point(12, 108);
            this.sfDataGrid1.Name = "sfDataGrid1";
            this.sfDataGrid1.Size = new System.Drawing.Size(867, 396);
            this.sfDataGrid1.TabIndex = 7;
            this.sfDataGrid1.Text = "sfDataGrid1";
            // 
            // buttonRapor
            // 
            this.buttonRapor.BackColor = System.Drawing.Color.YellowGreen;
            this.buttonRapor.Location = new System.Drawing.Point(360, 531);
            this.buttonRapor.Name = "buttonRapor";
            this.buttonRapor.Size = new System.Drawing.Size(145, 38);
            this.buttonRapor.TabIndex = 8;
            this.buttonRapor.Text = "RAPORU OLUŞTUR";
            this.buttonRapor.UseVisualStyleBackColor = false;
            this.buttonRapor.Click += new System.EventHandler(this.buttonRapor_Click);
            // 
            // buttonCustomers
            // 
            this.buttonCustomers.Location = new System.Drawing.Point(13, 13);
            this.buttonCustomers.Name = "buttonCustomers";
            this.buttonCustomers.Size = new System.Drawing.Size(75, 23);
            this.buttonCustomers.TabIndex = 9;
            this.buttonCustomers.Text = "Customers";
            this.buttonCustomers.UseVisualStyleBackColor = true;
            this.buttonCustomers.Click += new System.EventHandler(this.buttonCustomers_Click);
            // 
            // buttonCustomersFiltered
            // 
            this.buttonCustomersFiltered.Location = new System.Drawing.Point(13, 42);
            this.buttonCustomersFiltered.Name = "buttonCustomersFiltered";
            this.buttonCustomersFiltered.Size = new System.Drawing.Size(112, 23);
            this.buttonCustomersFiltered.TabIndex = 9;
            this.buttonCustomersFiltered.Text = "Customers Filtered";
            this.buttonCustomersFiltered.UseVisualStyleBackColor = true;
            this.buttonCustomersFiltered.Click += new System.EventHandler(this.buttonCustomersFiltered_Click);
            // 
            // FormTablo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(891, 581);
            this.Controls.Add(this.buttonCustomersFiltered);
            this.Controls.Add(this.buttonCustomers);
            this.Controls.Add(this.buttonRapor);
            this.Controls.Add(this.sfDataGrid1);
            this.Name = "FormTablo";
            this.Text = "FormTablo";
            this.Load += new System.EventHandler(this.FormTablo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.sfDataGrid1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Syncfusion.WinForms.DataGrid.SfDataGrid sfDataGrid1;
        private System.Windows.Forms.Button buttonRapor;
        private System.Windows.Forms.Button buttonCustomers;
        private System.Windows.Forms.Button buttonCustomersFiltered;
    }
}