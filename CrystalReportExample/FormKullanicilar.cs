﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Syncfusion.WinForms.DataGrid.Enums;
using Syncfusion.WinForms.DataGrid.Styles;

namespace CrystalReportExample
{
    public partial class FormKullanicilar : Form
    {
        DataBaseDataContext _db = new DataBaseDataContext();
        PrintHelper P = new PrintHelper();
        DataTable dt = new DataTable();

        public FormKullanicilar()
        {
            InitializeComponent();
        }

        private void FormKullanicilar_Load(object sender, EventArgs e)
        {
            HesaplarListele();
        }
        
        public void KullanicilarListe(DataGridView lst)
        {
            DataGridViewTextBoxColumn KategoriID = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn KategoriAdi = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn IslemTipi = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn CreateDate = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn CreatedBy = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn UpdateDate = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn UpdatedBy = new DataGridViewTextBoxColumn();
            lst.Columns.AddRange(new DataGridViewColumn[] {
                KategoriID,
                KategoriAdi,
                IslemTipi,
                CreateDate,
                CreatedBy,
                UpdateDate,
                UpdatedBy
            });
            KullanicilarListeFormat(lst);
        }

        public void KullanicilarListeFormat(DataGridView lst)
        {
            // KategoriID, KategoriAdi, IslemTipi, CreateDate, CreatedBy, UpdateDate, UpdatedBy

            DataGridViewCellStyle dataGridViewCellStyle1 = new DataGridViewCellStyle();
            dataGridViewCellStyle1.Alignment = DataGridViewContentAlignment.MiddleCenter;
            lst.Columns[0].DefaultCellStyle = dataGridViewCellStyle1;
            lst.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            lst.Columns[0].DefaultCellStyle = dataGridViewCellStyle1;
            lst.Columns[0].HeaderText = @"Kategori ID";
            lst.Columns[0].ReadOnly = true;
            lst.Columns[0].Width = 120;

            lst.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            lst.Columns[1].HeaderText = @"Kategori Adı";
            lst.Columns[1].ReadOnly = true;
            lst.Columns[1].Width = 120;

            lst.Columns[2].HeaderText = @"İşlem Tipi";
            lst.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            lst.Columns[2].Width = 150;
            lst.Columns[2].ReadOnly = true;

            lst.Columns[3].HeaderText = @"Oluşturulma Tarihi";
            lst.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            lst.Columns[3].Width = 120;
            lst.Columns[3].ReadOnly = true;

            lst.Columns[4].HeaderText = @"Oluşturan Kişi";
            lst.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            lst.Columns[4].Width = 120;
            lst.Columns[4].ReadOnly = true;


            lst.Columns[5].HeaderText = @"Güncellenme Tarihi";
            lst.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            lst.Columns[5].Width = 120;
            lst.Columns[5].ReadOnly = true;

            lst.Columns[6].HeaderText = @"Güncelleyen Kişi";
            lst.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            lst.Columns[6].Width = 120;
            lst.Columns[6].ReadOnly = true;

        }
        
        public void HesaplarListe(DataGridView lst)
        {
            DataGridViewCellStyle dataGridViewCellStyle1 = new DataGridViewCellStyle();
            dataGridViewCellStyle1.Alignment = DataGridViewContentAlignment.MiddleCenter;

            DataGridViewTextBoxColumn AltKategoriID = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn AltKategoriAdi = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn KategoriID = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn CreateDate = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn CreatedBy = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn UpdateDate = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn UpdatedBy = new DataGridViewTextBoxColumn();
           
            if (!(lst.ColumnCount > 0))
            {
                lst.Columns.AddRange(new DataGridViewColumn[] {
                    AltKategoriID,
                    AltKategoriAdi,
                    KategoriID,
                    CreateDate,
                    CreatedBy,
                    UpdateDate,
                    UpdatedBy
                });
            }

            lst.Columns[0].DefaultCellStyle = dataGridViewCellStyle1;
            lst.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            lst.Columns[0].DefaultCellStyle = dataGridViewCellStyle1;
            lst.Columns[0].HeaderText = @"Alt Kategori ID";
            lst.Columns[0].ReadOnly = true;
            lst.Columns[0].Width = 120;
            lst.Columns[0].Visible = false;

            lst.Columns[1].DefaultCellStyle = dataGridViewCellStyle1;
            lst.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            lst.Columns[1].DefaultCellStyle = dataGridViewCellStyle1;
            lst.Columns[1].HeaderText = @"Alt Kategori Adı";
            lst.Columns[1].ReadOnly = true;
            lst.Columns[1].Width = 120;

            lst.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            lst.Columns[2].HeaderText = @"Kategori Adı";
            lst.Columns[2].ReadOnly = true;
            lst.Columns[2].Width = 120;

            lst.Columns[3].HeaderText = @"Oluşturulma Tarihi";
            lst.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            lst.Columns[3].Width = 120;
            lst.Columns[3].ReadOnly = true;

            lst.Columns[4].HeaderText = @"Oluşturan Kişi";
            lst.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            lst.Columns[4].Width = 120;
            lst.Columns[4].ReadOnly = true;


            lst.Columns[5].HeaderText = @"Güncellenme Tarihi";
            lst.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            lst.Columns[5].Width = 120;
            lst.Columns[5].ReadOnly = true;

            lst.Columns[6].HeaderText = @"Güncelleyen Kişi";
            lst.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            lst.Columns[6].Width = 120;
            lst.Columns[6].ReadOnly = true;

        }

        private void HesaplarListele()
        {
            DataGridView virtualList = new DataGridView();

            var Lst = (from s in _db.Customers select s).ToList();

            HesaplarListe(virtualList);

            int i = 0;

            foreach (var t in Lst)
            {
                virtualList.Rows.Add();
                virtualList.Rows[i].Cells[0].Value = t.CustomerID;
                virtualList.Rows[i].Cells[1].Value = t.CompanyName;
                virtualList.Rows[i].Cells[2].Value = t.ContactName;
                virtualList.Rows[i].Cells[3].Value = t.ContactTitle;
                virtualList.Rows[i].Cells[4].Value = t.Address;
                virtualList.Rows[i].Cells[5].Value = t.City;
                virtualList.Rows[i].Cells[6].Value = t.Region;
                virtualList.Rows[i].Cells[7].Value = t.PostalCode;
                virtualList.Rows[i].Cells[8].Value = t.Country;
                virtualList.Rows[i].Cells[9].Value = t.Phone;
                virtualList.Rows[i].Cells[10].Value = t.Fax;
                i++;
            }
            virtualList.AllowUserToAddRows = false;
            virtualList.AllowUserToDeleteRows = false;
            virtualList.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            dt = P.GetDataTableFromDGV(virtualList);
            Liste2.DataSource = dt;
            HesaplarListe(Liste2);
            Liste2.Columns[11].Visible = false;
            Liste2.AllowUserToAddRows = false;
        }
    
        private void Btn_Yazdir_Click(object sender, EventArgs e)
        {
            Yaz();
        }

        private void Yaz()
        {
            FormPrint prnt = new FormPrint();
            prnt.HangiListe = "Hesaplar";
            prnt.Show();
        }

        private void textBoxSearch_TextChanged(object sender, EventArgs e)
        {
            dt.DefaultView.RowFilter = string.Format("[_RowString] LIKE '%{0}%'", textBoxSearch.Text);
        }
    }
}
