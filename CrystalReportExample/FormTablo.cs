﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CrystalReportExample
{
    public partial class FormTablo : Form
    {
        private string hangiListe = "";

        public FormTablo()
        {
            InitializeComponent();
        }

        private void FormTablo_Load(object sender, EventArgs e)
        {

        }

        private void buttonCustomers_Click(object sender, EventArgs e)
        {
            hangiListe = "Customers";
            DataBaseDataContext _db = new DataBaseDataContext();
            var list = (from s in _db.Customers select s).ToList();
            sfDataGrid1.DataSource = list;
        }

        private void buttonRapor_Click(object sender, EventArgs e)
        {
            FormPrint prnt = new FormPrint();
            prnt.HangiListe = hangiListe;
            prnt.Show();
        }

        private void buttonCustomersFiltered_Click(object sender, EventArgs e)
        {
            hangiListe = "CustomersFiltered";
            DataBaseDataContext _db = new DataBaseDataContext();
            var list = (from s in _db.Customers where s.Country.Contains("Venezuela") select s).ToList();
            sfDataGrid1.DataSource = list;
        }
    }
}
