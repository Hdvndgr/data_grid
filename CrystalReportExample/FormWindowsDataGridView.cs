﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CrystalReportExample
{
    public partial class FormWindowsDataGridView : Form
    {
        DataBaseDataContext _db = new DataBaseDataContext();
        PrintHelper P = new PrintHelper();
        DataTable dt = new DataTable();


        public FormWindowsDataGridView()
        {
            InitializeComponent();
        }

        private void FormWindowsDataGridView_Load(object sender, EventArgs e)
        {
            Listele();
        }

        public void CustomersListe(DataGridView lst)
        {
            DataGridViewCellStyle dataGridViewCellStyle1 = new DataGridViewCellStyle();
            dataGridViewCellStyle1.Alignment = DataGridViewContentAlignment.MiddleCenter;

            DataGridViewTextBoxColumn Col_0 = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn Col_1 = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn Col_2 = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn Col_3 = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn Col_4 = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn Col_5 = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn Col_6 = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn Col_7 = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn Col_8 = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn Col_9 = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn Col_10 = new DataGridViewTextBoxColumn();

            if (!(lst.ColumnCount > 0))
            {
                lst.Columns.AddRange(new DataGridViewColumn[] {
                    Col_0,
                    Col_1,
                    Col_2,
                    Col_3,
                    Col_4,
                    Col_5,
                    Col_6,
                    Col_7,
                    Col_8,
                    Col_9,
                    Col_10
                });
            }


            lst.Columns[0].HeaderText = @"Müşteri ID";
            lst.Columns[0].DefaultCellStyle = dataGridViewCellStyle1;
            lst.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            lst.Columns[0].Width = 120;
            lst.Columns[0].ReadOnly = true;

            lst.Columns[1].HeaderText = @"Şirket Adı";
            lst.Columns[1].DefaultCellStyle = dataGridViewCellStyle1;
            lst.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            lst.Columns[1].Width = 120;

            lst.Columns[2].HeaderText = @"Temsilci";
            lst.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            lst.Columns[2].Width = 120;

            lst.Columns[3].HeaderText = @"Temsilci Ünvanı";
            lst.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            lst.Columns[3].Width = 120;

            lst.Columns[4].HeaderText = @"Adres";
            lst.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            lst.Columns[4].Width = 120;

            lst.Columns[5].HeaderText = @"Şehir";
            lst.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            lst.Columns[5].Width = 120;

            lst.Columns[6].HeaderText = @"Bölge";
            lst.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            lst.Columns[6].Width = 120;

            lst.Columns[7].HeaderText = @"Posta Kodu";
            lst.Columns[7].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            lst.Columns[7].ReadOnly = true;
            lst.Columns[7].Width = 120;

            lst.Columns[8].HeaderText = @"Ülke";
            lst.Columns[8].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            lst.Columns[8].ReadOnly = true;
            lst.Columns[8].Width = 120;

            lst.Columns[9].HeaderText = @"Telefon";
            lst.Columns[9].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            lst.Columns[9].ReadOnly = true;
            lst.Columns[9].Width = 120;

            lst.Columns[10].HeaderText = @"Fax";
            lst.Columns[10].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            lst.Columns[10].ReadOnly = true;
            lst.Columns[10].Width = 120;
        }

        private void Listele()
        {
            DataGridView virtualList = new DataGridView();

            var Lst = (from s in _db.Customers select s).ToList();

            CustomersListe(virtualList);

            int i = 0;

            foreach (var t in Lst)
            {
                virtualList.Rows.Add();
                virtualList.Rows[i].Cells[0].Value = t.CustomerID;
                virtualList.Rows[i].Cells[1].Value = t.CompanyName;
                virtualList.Rows[i].Cells[2].Value = t.ContactName;
                virtualList.Rows[i].Cells[3].Value = t.ContactTitle;
                virtualList.Rows[i].Cells[4].Value = t.Address;
                virtualList.Rows[i].Cells[5].Value = t.City;
                virtualList.Rows[i].Cells[6].Value = t.Region;
                virtualList.Rows[i].Cells[7].Value = t.PostalCode;
                virtualList.Rows[i].Cells[8].Value = t.Country;
                virtualList.Rows[i].Cells[9].Value = t.Phone;
                virtualList.Rows[i].Cells[10].Value = t.Fax;
                i++;
            }
            virtualList.AllowUserToAddRows = false;
            virtualList.AllowUserToDeleteRows = false;
            virtualList.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            dt = P.GetDataTableFromDGV(virtualList);
            Liste2.DataSource = dt;
            CustomersListe(Liste2);
            Liste2.Columns[11].Visible = false;
            Liste2.AllowUserToAddRows = false;
        }

        private void textBoxSearch_TextChanged(object sender, EventArgs e)
        {
            dt.DefaultView.RowFilter = string.Format("[_RowString] LIKE '%{0}%'", textBoxSearch.Text);
        }
    }
}
