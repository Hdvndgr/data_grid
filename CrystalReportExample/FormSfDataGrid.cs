﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ColumnTypes;
using Syncfusion.Data.Extensions;
using Syncfusion.WinForms.DataGrid.Enums;
using Syncfusion.WinForms.DataGrid.Events;
using Syncfusion.WinForms.DataGrid.Interactivity;

namespace CrystalReportExample
{
    public partial class FormSfDataGrid : Form
    {
        DataBaseDataContext _db = new DataBaseDataContext();
        List<Deneme2> denemeList = new List<Deneme2>();
        Deneme2RowsCollection collection = new Deneme2RowsCollection();
        ObservableCollection<Deneme2Row> source = new ObservableCollection<Deneme2Row>();

        PrintHelper P = new PrintHelper();
        DataTable dt = new DataTable();
        Color oddRow;
        Color evenRow;

        public FormSfDataGrid()
        {
            InitializeComponent();
        }

        private void FormSfDataGrid_Load(object sender, EventArgs e)
        {
            dataGridSettings();
            //// Tüm listeyi aktarmak için
            //WholeList();

            //// Kısmi listeyi aktarma için
            //PartsOfList();

            //// ForeignKey içeren liste
            //ForeignKeyList();

            source = collection.GetList();
            sfDataGrid1.DataSource = source;
        }

        #region Settings

        // Satırları Renklendirme İşlemi
        private void SfDataGrid_QueryRowStyle(object sender, QueryRowStyleEventArgs e)
        {
            if (e.RowType == RowType.DefaultRow && e.RowIndex % 2 == 0)
                e.Style.BackColor = evenRow;
            else
                e.Style.BackColor = oddRow;
        }

        // Gruplama
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            sfDataGrid1.AllowGrouping = checkBox1.Checked;
        }

        // Grup Panel Visiblity
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            sfDataGrid1.GroupPanel.Visible = checkBox2.Checked;
        }

        // Düzenleme
        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            sfDataGrid1.AllowEditing = checkBox3.Checked;
        }

        // Sütun Sürükleme
        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            sfDataGrid1.AllowDraggingColumns = checkBox4.Checked;
        }

        // Silme
        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            sfDataGrid1.AllowDeleting = checkBox5.Checked;
        }

        // Filtreleme
        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            sfDataGrid1.AllowFiltering = checkBox6.Checked;
        }

        // Sıralama
        private void checkBox7_CheckedChanged(object sender, EventArgs e)
        {
            sfDataGrid1.AllowSorting = checkBox7.Checked;
        }

        // Sütun Boyut Ayarlama
        private void checkBox8_CheckedChanged(object sender, EventArgs e)
        {
            sfDataGrid1.AllowResizingColumns = checkBox8.Checked;
        }

        // Tıklayarak Seçme
        private void checkBox9_CheckedChanged(object sender, EventArgs e)
        {
            sfDataGrid1.AllowSelectionOnMouseDown = checkBox9.Checked;
        }

        // Üç Seviyeli Sıralama
        private void checkBox10_CheckedChanged(object sender, EventArgs e)
        {
            sfDataGrid1.AllowTriStateSorting = checkBox10.Checked;
        }

        // Show Row Header
        private void checkBox11_CheckedChanged(object sender, EventArgs e)
        {
            sfDataGrid1.ShowRowHeader = checkBox11.Checked;
        }

        // Show Header Tool Tip
        private void checkBox12_CheckedChanged(object sender, EventArgs e)
        {
            sfDataGrid1.ShowHeaderToolTip = checkBox12.Checked;
        }

        // Show Group Drop Area
        private void checkBox13_CheckedChanged(object sender, EventArgs e)
        {
            sfDataGrid1.ShowGroupDropArea = checkBox13.Checked;
        }

        // Show Tool Tip
        private void checkBox14_CheckedChanged(object sender, EventArgs e)
        {
            sfDataGrid1.ShowToolTip = checkBox14.Checked;
        }

        // Selection Mode
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    sfDataGrid1.SelectionMode = GridSelectionMode.None;
                    break;
                case 1:
                    sfDataGrid1.SelectionMode = GridSelectionMode.Single;
                    break;
                case 2:
                    sfDataGrid1.SelectionMode = GridSelectionMode.SingleDeselect;
                    break;
                case 3:
                    sfDataGrid1.SelectionMode = GridSelectionMode.Multiple;
                    break;
                case 4:
                    sfDataGrid1.SelectionMode = GridSelectionMode.Extended;
                    break;
            }
        }

        // Arama Filtreleme
        private void checkBox15_CheckedChanged(object sender, EventArgs e)
        {
            sfDataGrid1.SearchController.AllowFiltering = checkBox15.Checked;
            sfDataGrid1.View.RefreshFilter();
            sfDataGrid1.TableControl.Invalidate();
        }
        #endregion
        private void dataGridSettings()
        {
            evenRow = Color.AliceBlue;
            oddRow = Color.Transparent;
            sfDataGrid1.QueryRowStyle += SfDataGrid_QueryRowStyle;
            sfDataGrid1.AutoSizeColumnsMode = AutoSizeColumnsMode.Fill;
        }

        // Arama burada yapılıyor.
        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            sfDataGrid1.SearchController.Search(txtSearch.Text);
        }




        // Foreign Key yok ve tüm tabloyu yansıtıyoruz.
        private void WholeList()
        {
            var Lst = (from s in _db.Customers select s).ToList();
            sfDataGrid1.DataSource = Lst;
        }





        // Tablonun tamamını yansıtmak istemiyorsak.
        private void PartsOfList()
        {
            // CustomerID, CompanyName, ContactName, ContactTitle, Address, City, Region, PostalCode, Country, Phone, Fax
            var Lst = (from s in _db.Customers select s).ToList();
            sfDataGrid1.DataSource = Lst;
            sfDataGrid1.Columns[0].Visible = false;
            sfDataGrid1.Columns[4].Visible = false;
            sfDataGrid1.Columns[5].Visible = false;
            sfDataGrid1.Columns[6].Visible = false;
            sfDataGrid1.Columns[7].Visible = false;
            sfDataGrid1.Columns[8].Visible = false;
            sfDataGrid1.Columns[10].Visible = false;
        }





        // Foreign Key içeren Tabloyu yansıttık. (Table) yerine (View) kullandık.
        private void ForeignKeyList()
        {
            // Manuel yazma işlemini yapamadım. View oluşturmak zorunda kaldım. Böyle çok kolay.
            var Lst = (from s in _db.Denemes select s).ToList();
            sfDataGrid1.DataSource = Lst;
        }

        private void buttonEkle_Click(object sender, EventArgs e)
        {
            Deneme2Row de = new Deneme2Row();
            de.Ad = textBoxAd.Text;
            de.Soyad = textBoxSoyad.Text;
            de.Tarih = dateTimePicker1.Value;
            de.Sayi = Int32.Parse(textBoxSayi.Text);

            collection.AddRow(source, de);
            sfDataGrid1.Invalidate();
        }

        private void buttonKaydet_Click(object sender, EventArgs e)
        {
            DataBaseDataContext _dbb = new DataBaseDataContext();

            foreach (var att in source)
            {
                Deneme2 item;
                bool guncellemeModu = false;
                if (att.RowID != null)
                {
                    guncellemeModu = true;
                    item = _dbb.Deneme2s.First(d => d.ID == att.RowID);
                }
                else
                {
                    item = new Deneme2();
                }

                item.Ad = att.Ad;
                item.Soyad = att.Soyad;
                item.Tarih = att.Tarih;
                item.Sayi = att.Sayi;

                if (!guncellemeModu)
                {
                    _dbb.Deneme2s.InsertOnSubmit(item);
                }
                _dbb.SubmitChanges();
            }
            
            MessageBox.Show("Kayıt yapıldı.");
            source = collection.GetList();
            sfDataGrid1.Invalidate();
        }

        private void sfDataGrid1_RecordDeleting(object sender, RecordDeletingEventArgs e)
        {
            if (MessageBox.Show("Emin misin?", "Silme İşlemi",MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Sil();
            }
        }


        private void Sil()
        {
            try
            {
                DataBaseDataContext _db2 = new DataBaseDataContext();
                var reflector = this.sfDataGrid1.View.GetPropertyAccessProvider();
                var row = this.sfDataGrid1.SelectedItem;
                //Get the value from data object based on MappingName 
                var cellvalue = reflector.GetValue(row, "ID");
                //Returns the display value of the cell from data object based on MappingName 
                //var displayValue = reflector.GetFormattedValue(row, column.MappingName);
                int id = int.Parse(cellvalue.ToString());

                Deneme2 item = _db2.Deneme2s.First(d => d.ID == id);
                _db2.Deneme2s.DeleteOnSubmit(item);
                _db2.SubmitChanges();
                MessageBox.Show("Başarıyla Silindi");
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

    }

   

    public class Deneme2Row : INotifyPropertyChanged
    {
        private int? _RowID;

        private string _AD;

        private string _SOYAD;

        private DateTime _TARIH;

        private int? _SAYI;

        [Display(Name = "Row ID")]
        public int? RowID
        {
            get => this._RowID;
            set
            {
                this._RowID = value;
                this.OnPropertyChanged("RowID");
            }
        }

        [Display(Name = "AD")]
        public string Ad
        {
            get => this._AD;

            set
            {
                this._AD = value;
                this.OnPropertyChanged("Ad");
            }
        }

        [Display(Name = "SOYAD")]
        public string Soyad
        {
            get => this._SOYAD;
            set
            {
                this._SOYAD = value;
                this.OnPropertyChanged("Soyad");
            }
        }

        [Display(Name = "TARİH")]
        public DateTime Tarih
        {
            get => this._TARIH;
            set
            {
                this._TARIH = value;
                this.OnPropertyChanged("Tarih");
            }
        }

        [Display(Name = "SAYI")]
        public int? Sayi
        {
            get => this._SAYI;

            set
            {
                this._SAYI = value;
                this.OnPropertyChanged("Sayi");
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public Deneme2Row()
        {

        }

        public Deneme2Row(int id, string name, string surname, DateTime date, int? sayi)
        {
            RowID = id;
            Ad = name;
            Soyad = surname;
            Tarih = date;
            Sayi = sayi;
        }

    }

    public class Deneme2RowsCollection : IDisposable
    {
        private readonly DataBaseDataContext _db = new DataBaseDataContext();

        public ObservableCollection<Deneme2Row> DenemeRowsList { get; set; }

        public Deneme2RowsCollection()
        {
            DenemeRowsList = GetList();
        }


        public ObservableCollection<Deneme2Row> GetList()
        {
            var list = (from s in _db.Deneme2s select s).ToList();
            ObservableCollection<Deneme2Row> denemeRows = new ObservableCollection<Deneme2Row>();

            foreach (var v in list)
            {
                if (v.Tarih != null)
                {
                    Deneme2Row temp = new Deneme2Row(v.ID, v.Ad, v.Soyad, v.Tarih.Value, v.Sayi);
                    denemeRows.Add(temp);
                }
            }

            return denemeRows;
        }

        public ObservableCollection<Deneme2Row> AddRow(ObservableCollection<Deneme2Row> source, Deneme2Row item)
        {
            source.Add(item);
            return source;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool isdisposable)
        {
            DenemeRowsList?.Clear();
        }

    }


}
